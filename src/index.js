import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App.jsx";

import { ChainId, ThirdwebProvider } from '@thirdweb-dev/react';

ReactDOM.render(
  <React.StrictMode>
    <ThirdwebProvider desiredChainId={ChainId.Rinkeby}>
      <App />
    </ThirdwebProvider>
  </React.StrictMode>,
  document.getElementById("root")
);
