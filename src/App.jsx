import { useState, useEffect } from 'react';
import { useAddress, useMetamask, useEditionDrop, useToken } from '@thirdweb-dev/react';

const editionDropContractAddress = '0x4141e2573854AFe919784972bA0a371d6B6A41ff';
const tokenContractAddress = '0x7F21dC43270EC39c49E0500146309090E15412ea';
// const voteContractAddress = '0xF1D4a7A6A9F4Cc20e9aCa5Ff61d5f3A1Be3d641C';
const myWallets = [
  '0x1F1c96B177Cb49791Bf13b16cBE3461089af475e', // main
  '0x8078c2dD67e11BDe3e4A5F1D87312744bC82fe0B', // test1
  '0x90B945800708BbD1c874C67C3Cb401B579F7750e', // test2
]

const App = () => {
  const [isMember, setIsMember] = useState(false);
  const [isClaiming, setIsClaiming] = useState(false);
  const [memberAddresses, setMemberAddresses] = useState(myWallets);
  const [memberTokenAmounts, setMemberTokenAmounts] = useState()
  // const [proposals, setProposals] = useState([]);
  // const [isVoting, setIsVoting] = useState(false);
  // const [hasVoted, setHasVoted] = useState(false);

  const connectWithMetamask = useMetamask();
  const currentWalletAddress = useAddress();
  const editionDropContract = useEditionDrop(editionDropContractAddress);
  const tokenContract = useToken(tokenContractAddress);
  // const voteContract = useVote(voteContractAddress);

  const shortenAddress = (str) => {
    return str.substring(2,6) + '...' + str.substring(str.length-4);
  };

  // Check if the current user has a membership NFT.
  useEffect(() => {
    if (!currentWalletAddress) {
      return;
    };
    async function fetchNFTBalance() {
      const balance = await editionDropContract.balanceOf(currentWalletAddress, '0');
      if(balance.gt(0)) {
        setIsMember(true);
      } else {
        setIsMember(false);
      }
    }
    fetchNFTBalance();
  }, [currentWalletAddress, editionDropContract]);

  // Check all holders of the membership NFT, and how many CHESS tokens each has.
  useEffect(() => {
    async function getMemberTokenAmounts() {
      const data = await Promise.all(memberAddresses.map(async address => {
        const output = await tokenContract.balanceOf(address);
        return {address, tokenAmount: output.value};
      }));
      setMemberTokenAmounts(data);
    }
    getMemberTokenAmounts();
  }, [memberAddresses, tokenContract]);

  // useEffect(() => {
  //   async function fetchProposals() {
  //     const currentProposals = await voteContract.getAll();
  //     setProposals(currentProposals);
  //   }
  //   fetchProposals();
  // });

  const mintNft = async () => {
    setIsClaiming(true);
    try {
      await editionDropContract.claim(0, 1);
      setIsMember(true);
      console.log(`Successfully Minted! Check it out on OpenSea: https://testnets.opensea.io/assets/${editionDropContractAddress}/0`);
    } catch (error) {
      console.error("Failed to claim", error);
    } finally {
      setIsClaiming(false);
    }
  }

  const renderConnected = () => {
    if (isMember) {
      return (
        <div className="member-page">
          <h4>Congratulations on being a member</h4>
          {/* <div>
            <h4>Member List</h4>
            <table className="card">
              <thead>
                <tr>
                  <th>Address</th>
                  <th>Token Amount</th>
                </tr>
              </thead>
              <tbody>
                {memberTokenAmounts.map(member => (
                  <tr key={member.address}>
                    <td>{shortenAddress(member.address)}</td>
                    <td>{member.tokenAmount}</td>
                  </tr>
                ))}
              </tbody>
            </table>
          </div> */}
        </div>
      )
    } else {
      return (
        <div className="mint-nft">
          <h4>Mint your free ChessDAO Membership NFT</h4>
          <button disabled={isClaiming} onClick={() => mintNft()}>
            {isClaiming ? "Minting..." : "Mint"}
          </button>
        </div>
      )
    }
  }

  return (
    <div className="landing">
      <h1>Welcome to ChessDAO</h1>
      <p>........................................................</p>
      <h2>Wallet Area</h2>
      {currentWalletAddress
        ? <h4>Connected as {shortenAddress(currentWalletAddress)}</h4>
        : <button onClick={connectWithMetamask} className="btn-hero">Connect Metamask Wallet</button>
      }
      <p>........................................................</p>
      <h2>Membership Area</h2>
      {currentWalletAddress && renderConnected()}
    </div>
  )
};

export default App;
