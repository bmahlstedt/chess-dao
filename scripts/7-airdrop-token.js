import { contractToken } from './5-deploy-token.js';

// Transfer to main account and then test account 2.
// Remember test account 1 is the account I minted everything to.
const data = [
  {
    toAddress: '0x1F1c96B177Cb49791Bf13b16cBE3461089af475e',
    amount: 900,
  },
  {
    toAddress: '0x90B945800708BbD1c874C67C3Cb401B579F7750e',
    amount: 90,
  },
]

await contractToken.transferBatch(data);