import { sdk } from './1-initialize-sdk.js';

// Created this contract from the thirdweb v2 UI.
const address = '0xF1D4a7A6A9F4Cc20e9aCa5Ff61d5f3A1Be3d641C';

export const contractVote = sdk.getVote(address);