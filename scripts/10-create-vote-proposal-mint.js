import { contractToken } from './5-deploy-token.js';
import { contractVote } from './8-deploy-vote.js';

const description = "Mint 1,000,000 additional CHESS tokens into the treasury."
const executions = [
  {
    toAddress: contractToken.getAddress(),
    nativeTokenValue: 0, // native is ETH; no ETH transfers here, just CHESS token
    transactionData: contractToken.encoder.encode(
      "mintTo",
      [
        contractVote.getAddress(),
        1000000,
      ]
    ),
  }
]

await contractVote.propose(description, executions);