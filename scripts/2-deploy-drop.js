import { sdk } from './1-initialize-sdk.js';

// Created this contract from the thirdweb v2 UI.
const address = '0x4141e2573854AFe919784972bA0a371d6B6A41ff';

export const contractEditionDrop = sdk.getEditionDrop(address);