import { contractToken } from './5-deploy-token.js';
import { contractVote } from './8-deploy-vote.js';
import { ethers } from 'ethers';

const description = "Transfer 1000 CHESS tokens to Brian."
const executions = [
  {
    toAddress: contractToken.getAddress(),
    nativeTokenValue: 0, // native is ETH; no ETH transfers here, just CHESS token
    transactionData: contractToken.encoder.encode(
      "transfer",
      [
        process.env.WALLET_ADDRESS,
        ethers.utils.parseUnits('1000', 18),
      ]
    ),
  }
]

await contractVote.propose(description, executions);