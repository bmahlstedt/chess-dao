import { contractEditionDrop } from './2-deploy-drop.js';

const claimConditions = [
  {
    startTime: new Date(), // start the sale now
    maxQuantity: 50000, // limit how many mints
  }
];
const tokenId = 0; // the id of the NFT to set claim conditions on

await contractEditionDrop.claimConditions.set(tokenId, claimConditions);