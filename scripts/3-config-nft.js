import { contractEditionDrop } from './2-deploy-drop.js';
import { readFileSync } from 'fs';

const metadatas = [{
  name: 'Black Knight',
  description: 'This steed provides access to ChessDAO',
  image: readFileSync('scripts/assets/chess-knight.jpg'),
}];

await contractEditionDrop.createBatch(metadatas);