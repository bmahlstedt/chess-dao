import { ThirdwebSDK } from '@thirdweb-dev/sdk';
import ethers from 'ethers';
import dotenv from 'dotenv';

dotenv.config();

export const sdk = new ThirdwebSDK(
  new ethers.Wallet(
    process.env.PRIVATE_KEY,
    ethers.getDefaultProvider(process.env.ALCHEMY_API_URL),
  ),
);