import { sdk } from './1-initialize-sdk.js';

// Created this contract from the thirdweb v2 UI.
const address = '0x7F21dC43270EC39c49E0500146309090E15412ea';

export const contractToken = sdk.getToken(address);