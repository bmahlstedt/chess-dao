import { contractToken } from './5-deploy-token.js';
import { contractVote } from './8-deploy-vote.js';

// Manually gave this vote (treasury) address minting rights
// on the token contract via the UI.
// https://thirdweb.com/dashboard/rinkeby/token/0x7F21dC43270EC39c49E0500146309090E15412ea?tabIndex=1

// Then transferred the majority of my CHESS token to the treasury.
await contractToken.transfer(contractVote.getAddress(), 900000);